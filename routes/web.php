<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/joboffer', function () {
    return view('jobOffer');
});

Route::get('/aboutUs', function () {
    return view('aboutUs');
});

Route::get('/homepage', 'Controller@homepage');
Route::get('/contact', 'contactController@show');
Route::get('/jobDetails', 'jobOffersController@showDetails');
Route::get('/profil', 'profilController@show');
Route::get('/admin', 'AdminController@show');
Route::get('/logout', 'LogoutController@logout');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::resource('candidates', 'CandidatesController');
Route::resource('clients', 'ClientsController');
Route::resource('joboffers', 'jobOffersController');
Route::resource('candidacys', 'CandidacysController');

Route::post('/send', 'contactController@send')->name('send');

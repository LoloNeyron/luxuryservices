<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Candidates extends Model 
{

    protected $table = 'candidates';
    protected $primaryKey = 'id';
    protected $fillable = ['genre', 'firstName', 'lastName', 'Adress', 'country', 'nationality', 'passportUpload', 'cv', 'picProfile', 'location', 'dateOfBirth', 'placeOfBirth', 'email', 'confirmEmail', 'password', 'confirmPassword', 'Availability', 'jobSector', 'experience', 'notes', 'delete_at', 'file'];
    protected $dates = ['created_at', 'updated_at'];

    public function candidatesCandidacys()
    {
        return $this->hasMany('App\Candidacys');
    }

    public function getName() {
        return $this->firstName;
    }

}
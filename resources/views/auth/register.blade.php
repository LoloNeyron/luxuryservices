@extends('layouts.base')
@section('title', 'Register')
@section('content')


<style>
    .page-title-bg {
        background-image: url(../../assets/img/bg3.jpg);
    }
    .log-section form .checkbox label {
        font-size: 16px
    }
    .log-section form .checkbox label a {
        color: #b6a575;
        text-decoration: underline;
        text-transform: inherit;
        font-size: 16px
    }
    .log-section form .links a {
        display: block;
        text-align: center;
        color: #b6a575;
        text-decoration: underline;
        text-transform: inherit;
        font-size: 16px
    }
</style>

<!-- Page Header-->
<section class="page-title page-title-bg fixed-bg overlay dark-5 padding-top-160 padding-bottom-80">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="white-text">Register</h2>
                    <span class="white-text">Welcome on board</span>
                    <ol class="breadcrumb">
                        <li><a href="homepage">Home</a></li>
                        <li class="active">Register</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>


    <!-- Page Content-->
    <section class="section-padding gray-bg log-section">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-sm-offset-3 card card-panel">
                    <h3 class="text-extrabold">Create a new account</h3>
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form id="register-form" role="form" method="post" accept-char="UTF-8" autocomplete="off" data-parsley-validate>
                        @csrf
                        
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="input-field">
                            <input type="email" name="email" id="email" value=""
                                   required
                                   data-parsley-trigger="change"
                                   data-parsley-error-message="A valid email address is required.">
                            <label for="email">Email</label>
                            <span class="help-block">Type your email address.</span>
                        </div>
                        <div class="input-field">
                            <input type="password" name="password" id="password" value=""
                                   required
                                   data-parsley-trigger="change"
                                   data-parsley-minlength="6"
                                   data-parsley-error-message="The password must be at least 6 characters.">
                            <label for="password">Password</label>
                            <i class="fa fa-eye show-password"></i>
                            <span class="help-block">Type your password.</span>
                        </div>
                        <div class="input-field">
                            <input type="password" name="password_confirmation" id="password_confirmation" value=""
                                   required
                                   data-parsley-trigger="change"
                                   data-parsley-equalto="#password"
                                   data-parsley-error-message="Password does not match.">
                            <label for="password_confirmation">Confirm Password</label>
                            <i class="fa fa-eye show-password"></i>
                            <span class="help-block">Confirm your password.</span>
                        </div>
                        <!-- ------------------------------- -->
                        <div class="input-field">
                            <input type="text" name="name" id="name" value=""
                                    required
                                    data-parsley-trigger="change">
                            <label for="name">Name</label>
                            <span class="help-block">Name</span>
                        </div>
                        <div class="input-field">
                            <input type="text" name="lastname" id="lastname" value=""
                                    required
                                    data-parsley-trigger="change">
                            <label for="lastname">last Name</label>
                            <span class="help-block">last Name</span>
                        </div>
                        <div class="input-field">
                                <label for="genre">Genre</label>
                                <br>
                                <select id="genre" type="select" class="form-control{{ $errors->has('genre') ? ' is-invalid' : '' }}" name="genre" value="{{ old('genre') }}" required autofocus>
                                    <option selected value="Male">Male</option>
                                    <option value="Female">Female</option>
                                </select>
                            <span class="help-block">Genre</span>
                        </div>

                        <div class="input-field">
                            <input type="text" name="adresse" id="adresse" value=""
                                    required
                                    data-parsley-trigger="change">
                            <label for="adresse">Adresse</label>
                            <span class="help-block">Adresse</span>
                        </div>
                        <div class="input-field">
                            <input type="text" name="country" id="country" value=""
                                    required
                                    data-parsley-trigger="change">
                            <label for="country">Country</label>
                            <span class="help-block">Country</span>
                        </div>
                        <div class="input-field">
                            <input type="text" name="nationality" id="nationality" value=""
                                    required
                                    data-parsley-trigger="change">
                            <label for="nationality">Nationality</label>
                            <span class="help-block">Nationality</span>
                        </div>

                        <div class="input-field">
                            <div class="checkbox">
                                <input type="checkbox" name="validPassport" value="1" id="validPassport" required/>
                                <label for="validPassport">Valid passport ?</label>
                            </div>
                        </div>
                        <div class="input-field">
                            <label for="passport">Passport</label>
                            <br>
                            <input type="file" name="passportFile" id="passportFile" value=""
                                    required
                                    data-parsley-trigger="change">
                            <span class="help-block">Passport</span>
                        </div>
                        <div class="input-field">
                            <label for="cv">Curriculum Vitae</label>
                            <br>
                            <input type="file" name="cv" id="cv" value=""
                                    required
                                    data-parsley-trigger="change">
                            <span class="help-block">cv</span>
                        </div>

                        <div class="input-field">
                            <label for="Profilpicture">Profil picture</label>
                            <br>
                            <input type="file" name="Profilpicture" id="Profilpicture" value=""
                                    required
                                    data-parsley-trigger="change">
                            <span class="help-block">Profil picture</span>
                        </div>

                        <div class="input-field">
                            <input type="text" name="currentlocation" id="currentlocation" value=""
                                    required
                                    data-parsley-trigger="change">
                            <label for="currentlocation">Current Location</label>
                            <span class="help-block">Current Location</span>
                        </div>

                        <div class="input-field">
                            <label for="dateOfBirth">Date Of Birth</label>
                            <br>
                            <input type="date" name="dateOfBirth" id="dateOfBirth" value=""
                                    required
                                    data-parsley-trigger="change">
                            <span class="help-block">Date Of Birth</span>
                        </div>

                        <div class="input-field">
                            <input type="text" name="placeOfBirth" id="placeOfBirth" value=""
                                    required
                                    data-parsley-trigger="change">
                            <label for="placeOfBirth">Place Of Birth</label>
                            <span class="help-block">Place Of Birth</span>
                        </div>

                        <div class="checkbox">
                            <input type="checkbox" name="Availability" value="1" id="Availability" required/>
                            <label for="Availability">Availability ?</a></label>
                        </div>

                        <div class="input-field">
                                <label for="jobSector">jobSector</label>
                                <br>
                                <select id="JobSector" type="select" class="form-control{{ $errors->has('JobSector') ? ' is-invalid' : '' }}" name="JobSector" value="{{ old('JobSector') }}" required autofocus>
                                    <option selected value="Commercial">Commercial</option>
                                    <option value="RetailSales">Retail sales</option>
                                    <option value="Creative">Creative</option>
                                    <option value="Technology">Technology</option>
                                    <option value="Marketing & PR">Marketing & PR</option>
                                    <option value="Fashion & Luxury">Fashion & luxury</option>
                                    <option value="Management & HR">Management & HR</option>
                                </select>
                            <span class="help-block">jobSector</span>
                        </div>

                        <div class="input-field">
                                <label for="experience">experience</label>
                                <br>
                                <select id="experience" type="select" class="form-control{{ $errors->has('experience') ? ' is-invalid' : '' }}" name="experience" value="{{ old('experience') }}" required autofocus>
                                        <option selected value="0 - 6 month">0 - 6 month</option>
                                        <option value="6 month - 1 year">6 month - 1 year</option>
                                        <option value="1 - 2 years">1 - 2 years</option>
                                        <option value="2+ years">2+ years</option>
                                        <option value="5+ years">5+ years</option>
                                        <option value="10+ years">10+ years</option>
                                </select>
                            <span class="help-block">experience</span>
                        </div>

                        <div class="input-field">
                            <label for="description">Short Description</label>
                            <br>
                            <textarea type="text" name="description" id="description" value=""
                                    required
                                    cols="20"
                                    rows="5"
                                    data-parsley-trigger="change"></textarea>
                            <span class="help-block">Short Description</span>
                        </div>

                        <div class="checkbox">
                            <input type="checkbox" name="accept-terms" value="1" id="accept-terms" required/>
                            <label for="accept-terms">I have read and I accept the <a href="#!" target="_blank">Terms Of Use</a></label>
                        </div>

                        <button type="submit" class="btn btn-lg gradient secondary btn-block waves-effect waves-light mt-20 mb-20">Create an account</button>
                        <div class="links">
                            <a href="home"">Already have an account? Click here</a>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </section>
@endsection

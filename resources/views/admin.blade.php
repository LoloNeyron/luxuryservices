@extends('layouts.base')
@section('title', 'Profil')
@section('content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
    <section class="gray-bg">
        <div class="container">                
        </div>
    </section>
    <section class="gray-bg">
            <div class="container">
                <div class="row">
                    <div class="score-container">
                        <br><br>
                        <h3>
                            Admin Space
                        </h3>
                    </div>
                </div>
            </div>
            <br><hr>
        </section>
        <section class="gray-bg">
            <div class="container-fluid">
                <div class="row">
                    <div class="col s2"  style="border-right: 1px solid #5b5b5b; box-shadow: 2px 2px 6px black">
                        <ul>
                            <li><a href="#!">option 1</a></li>
                            <li><a href="#!">option 2</a></li>
                            <li><a href="#!">option 3</a></li>
                            <li><a href="#!">option 4</a></li>
                            <li><a href="#!">option 5</a></li>
                            <li><a href="#!">option 6</a></li>
                            <li><a href="#!">option 7</a></li>
                            <li><a href="#!">option 8</a></li>
                        </ul>
                    </div>
                    <div class="col s10" style="height 20px">
                        <canvas id="myChart" height="10" style="border: 1px solid black; width: 100%"></canvas>
                    </div>
                </div>
            </div>
            <br><br>
            </section>
            <section>
                <div class="row">
                    <div class="col s12">
                        <ul class="tabs">
                            <li class="tab col s6"><a class="active"  href="#createJobs">Création d'une offre d'emploi</a></li>
                            <li class="tab col s6"><a href="#test2">Test 2</a></li>
                        </ul>
                    </div>
                    <div id="createJobs" class="col s12">
                        <form method="POST" action="#!" accept-charset="UTF-8" id="candidateForm" role="form" data-parsley-validate="" enctype="multipart/form-data"><input name="_token" type="hidden" value="UYfchy67WuTVzhSstPK1RDZEIfgqpKCcLnLyhf8a">
                            <section class="section-padding">
                                <div class="container">
                                    <div class="row">
                                        <h3 class="text-extrabold">Création d'une offre d'emploi</h3>
                                        <div class="clearfix visible-sm"></div>
                                        <div class="col-xs-12 col-sm-6 col-md-4">
                                            <div class="input-field">
                                                <input type="text" class="form-control" name="Society_Name" id="Society_Name" value="" required>
                                                <label for="Society_name">Society Name</label>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-4">
                                            <div class="input-field">
                                                <input type="text" class="form-control" name="employer_name" id="employer_name" value="" required>
                                                <label for="Employer_name">Employer name</label>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-4">
                                            <div class="input-field">
                                                <input type="text" class="form-control" name="job_title" id="job_title" value="" required>
                                                <label for="job_title">Job Title</label>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-4">
                                            <div class="input-field">
                                                <input type="text" class="form-control" name="salary" id="salary" value="" required>
                                                <label for="salary">Salary</label>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-4">
                                            <div class="input-field">
                                                <input required="required" id="email_employer" name="email_employer" type="text" value="">
                                                <label for="email_employer">Email Employer</label>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-4">
                                            <div class="input-field">
                                                <label for="employer_phone">Employer Phone</label>
                                                <br>
                                                <input required="required" id="employer_phone" name="employer_phone" type="phone" value="">
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12">
                                            <div class="input-field">
                                                <textarea class="materialize-textarea" id="description" name="description" cols="50" rows="10"></textarea>
                                                <label for="description">Short description for your offers </label>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6">
                                            <div class="input-field">
                                                <label for="jobSector">job Sector</label>
                                                <br>
                                                <select id="JobSector" type="select" class="form-control{{ $errors->has('JobSector') ? ' is-invalid' : '' }}" name="JobSector" value="{{ old('JobSector') }}" required autofocus>
                                                    <option selected value="Commercial">Commercial</option>
                                                    <option value="RetailSales">Retail sales</option>
                                                    <option value="Creative">Creative</option>
                                                    <option value="Technology">Technology</option>
                                                    <option value="Marketing & PR">Marketing & PR</option>
                                                    <option value="Fashion & Luxury">Fashion & luxury</option>
                                                    <option value="Management & HR">Management & HR</option>
                                                </select>
                                                <span class="help-block">jobSector</span>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6">
                                            <div class="input-field">
                                                <label for="jobType">job Type</label>
                                                <br>
                                                <select id="jobType" type="select" class="form-control{{ $errors->has('jobType') ? ' is-invalid' : '' }}" name="jobType" value="{{ old('jobType') }}" required autofocus>
                                                    <option selected value="Fulltime">Fulltime</option>
                                                    <option value="Parttime">Parttime</option>
                                                    <option value="Temporary">Temporary</option>
                                                    <option value="Freelance">Freelance</option>
                                                    <option value="Seasonal">Seasonal</option>
                                                </select>
                                                <span class="help-block">jobType</span>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            <div class="input-field">
                                                <input id="location" name="location" type="text" value="">
                                                <label for="location">location</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <div class="col-xs-12 col-sm-6 col-md-4 col-md-offset-4">
                                <button type="submit" class="btn btn-block btn-lg gradient secondary waves-effect waves-light">
                                    <span><strong>CREATE</strong></span>
                                </button>
                            </div>
                        </form>
                    </div>
                    <div id="test2" class="col s12">Test 2</div>
                </div>
            </section>
            












            














            <script>
                var ctx = document.getElementById('myChart').getContext('2d');
                var chart = new Chart(ctx, {
                    // The type of chart we want to create
                    type: 'line',

                    // The data for our dataset
                    data: {
                        labels: ["January", "February", "March", "April", "May", "June", "July"],
                        datasets: [{
                            label: "My First dataset",
                            backgroundColor: 'rgb(255, 99, 132)',
                            borderColor: 'rgb(255, 99, 132)',
                            data: [0, 10, 5, 2, 20, 30, 45],
                        }]
                    },

                    // Configuration options go here
                    options: {}
                });

            </script>
@endsection
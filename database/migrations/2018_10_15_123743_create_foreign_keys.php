<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateForeignKeys extends Migration {

	public function up()
	{
		Schema::table('jobOffers', function(Blueprint $table) {
			$table->foreign('client_id')->references('id')->on('clients')
						->onDelete('cascade')
						->onUpdate('restrict');
		});
		Schema::table('candidacys', function(Blueprint $table) {
			$table->foreign('candidates_id')->references('id')->on('candidates')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('candidacys', function(Blueprint $table) {
			$table->foreign('jobOffer_id')->references('id')->on('jobOffers')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
	}

	public function down()
	{
		Schema::table('jobOffers', function(Blueprint $table) {
			$table->dropForeign('jobOffers_client_id_foreign');
		});
		Schema::table('candidacys', function(Blueprint $table) {
			$table->dropForeign('candidacys_candidates_id_foreign');
		});
		Schema::table('candidacys', function(Blueprint $table) {
			$table->dropForeign('candidacys_jobOffer_id_foreign');
		});
	}
}